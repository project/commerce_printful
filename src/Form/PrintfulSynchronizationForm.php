<?php

namespace Drupal\commerce_printful\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\commerce_printful\PrintfulSyncBatch;

/**
 * Defines the Printful synchronization form.
 */
class PrintfulSynchronizationForm extends FormBase {

  /**
   * Printful stores.
   *
   * @var array
   */
  protected $printfulStores;

  /**
   * Constructor.
   */
  public function __construct(EntityTypeManagerInterface $entityTypeManager) {
    $this->printfulStores = $entityTypeManager->getStorage('printful_store')->loadMultiple();
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'commerce_printful_synchronization_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $stores_options = [];
    foreach ($this->printfulStores as $store_id => $store) {
      $stores_options[$store_id] = $store->get('label');
    }

    $bundle = [];
    if ($store = $form_state->getValue('printful_store_id')) {
      $bundle = [$this->printfulStores[$store]->get('productBundle')];
    }

    $form['printful_store_id'] = [
      '#type' => 'select',
      '#title' => $this->t('Printful store to be synchronized'),
      '#options' => $stores_options,
      '#required' => TRUE,
      '#ajax' => [
        'callback' => '::loadProductBundle',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'printful-product-list',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['update'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Update existing data'),
      '#default_value' => FALSE,
      '#description' => $this->t('If checked, existing products and variations will be updated, otherwise only new items will be imported.'),
      '#ajax' => [
        'callback' => '::loadProductBundle',
        'disable-refocus' => FALSE,
        'event' => 'change',
        'wrapper' => 'printful-product-list',
        'progress' => [
          'type' => 'throbber',
        ],
      ],
    ];

    $form['printful_product_id'] = [
      '#title' => $this->t('Select a Printful product to be synchronized'),
      '#type' => 'entity_autocomplete',
      '#description' => $this->t('Leave blank to update all products.'),
      '#target_type' => 'commerce_product',
      '#selection_handler' => 'default',
      '#prefix' => '<div id="printful-product-list">',
      '#suffix' => '</div>',
      '#selection_settings' => [
        'target_bundles' => $bundle,
      ],
      '#states' => [
        'visible' => [
          '#edit-update' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['execute_sync'] = [
      '#type' => 'submit',
      '#value' => $this->t('Execute'),
    ];

    return $form;
  }

  /**
   * Ajax callback event.
   *
   * @param array $form
   *  The triggering form render array.
   * @param Drupal\Core\Form\FormStateInterface $form_state
   *  Form state of current form.
   * @return mixed
   *  Must return AjaxResponse object or render array.
   *  Never return NULL or invalid render arrays. This
   *  could/will break your forms.
   */
  public function loadProductBundle(array &$form, FormStateInterface $form_state) {

    if ($store = $form_state->getValue('printful_store_id')) {
      if ($update = $form_state->getValue('update')) {
        $form_state->setRebuild(TRUE);
        return $form['printful_product_id'];
      }
    }

    return [
      '#prefix' => '<div id="printful-product-list">',
      '#suffix' => '</div>',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    batch_set(PrintfulSyncBatch::getBatch([
      'printful_store_id' => $form_state->getValue('printful_store_id'),
      'update' => $form_state->getValue('update'),
      'printful_product_id' => $form_state->getValue('printful_product_id'),
    ]));
  }

}
