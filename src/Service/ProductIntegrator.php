<?php

namespace Drupal\commerce_printful\Service;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Utility\Token;
use Drupal\commerce_printful\Entity\PrintfulStoreInterface;
use Drupal\commerce_product\Entity\ProductInterface;
use Drupal\commerce_printful\Exception\PrintfulException;
use Drupal\commerce_price\Price;
use Drupal\commerce_product\Entity\ProductVariationInterface;
use Drupal\file\FileRepository;
use Drupal\media\Entity\Media;

/**
 * Printful product integration service implementation.
 */
class ProductIntegrator implements ProductIntegratorInterface {

  use StringTranslationTrait;

  /**
   * The printful API service.
   *
   * @var \Drupal\commerce_printful\Service\PrintfulInterface
   */
  protected $pf;

  /**
   * The Entity Type Manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The file system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Logger for this service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * Drupal token service container.
   *
   * @var \Drupal\Core\Utility\Token
   */
  protected $token;

  /**
   * Commerce store entity.
   *
   * @var \Drupal\commerce_store\Entity\StoreInterface
   */
  protected $store;

  /**
   * Printful store config entity.
   *
   * @var \Drupal\commerce_printful\Entity\PrintfulStoreInterface
   */
  protected $printfulStore;

  /**
   * Should existing content be updated?
   *
   * @var bool
   */
  protected $update;

  /**
   * Provides a file entity repository.
   *
   * @var \Drupal\file\FileRepository
   */
  protected $fileRepository;

  /**
   * Constructor.
   *
   * @param \Drupal\commerce_printful\Service\PrintfulInterface $pf
   *   The printful API service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The Entity Type Manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   The logger factory.
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entity_field_manager
   *   The entity field manager.
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   * @param \Drupal\file\FileRepository $fileRepository
   *   Provides a file entity repository.
   */
  public function __construct(
    PrintfulInterface $pf,
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    LoggerChannelFactoryInterface $logger_factory,
    EntityFieldManagerInterface $entity_field_manager,
    Token $token,
    FileRepository $fileRepository
  ) {
    $this->pf = $pf;
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->logger = $logger_factory->get('commerce_printful');
    $this->entityFieldManager = $entity_field_manager;
    $this->token = $token;
    $this->fileRepository = $fileRepository;
  }

  /**
   * {@inheritdoc}
   */
  public function setConnectionInfo(array $data) {
    $this->pf->setConnectionInfo($data);
  }

  /**
   * {@inheritdoc}
   */
  public function setStore($store_id) {
    $this->store = $this->entityTypeManager->getStorage('commerce_store')->load($store_id);
  }

  /**
   * {@inheritdoc}
   */
  public function setPrintfulStore(PrintfulStoreInterface $printful_store) {
    $this->printfulStore = $printful_store;
    $this->setStore($printful_store->get('commerceStoreId'));
    $this->setConnectionInfo(['api_key' => $printful_store->get('apiKey')]);
  }

  /**
   * {@inheritdoc}
   */
  public function setUpdate($value) {
    $this->update = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSyncProducts($offset, $limit, $product_id = NULL) {
    if (!empty($product_id)) {
      $product = $this->entityTypeManager->getStorage('commerce_product')->load($product_id);
      if (isset($product->printful_reference)) {
        return $this->pf->syncProducts('@' . $product->printful_reference->first()->printful_id);
      }
    }

    return $this->pf->syncProducts(['offset' => $offset, 'limit' => $limit]);
  }

  /**
   * {@inheritdoc}
   */
  public function syncProduct(array $data) {
    $productStorage = $this->entityTypeManager->getStorage('commerce_product');
    $products = $productStorage->loadByProperties(['printful_reference' => $data['external_id']]);

    if (empty($products)) {
      // Create the new product.
      $product = $productStorage->create([
        'type' => $this->printfulStore->get('productBundle'),
        'title' => $data['name'],
        'printful_reference' => $data['external_id'],
        'stores' => $this->store,
      ]);
      $product->save();
    }
    else {
      $product = reset($products);
      if ($this->update) {
        $product->title->value = $data['name'];
        $product->save();
      }
    }

    return $product;
  }

  /**
   * {@inheritdoc}
   */
  public function syncProductVariants(ProductInterface $product, $sync_variants = NULL) {
    if (empty($sync_variants)) {
      try {
        $printful_id = $product->printful_reference->printful_id;
      }
      catch (\Exception $e) {
        $printful_id = '';
      }
      if (empty($printful_id)) {
        throw new PrintfulException(sprintf('Product %d is not synchronized with Printful.', $product->id()));
      }

      $old_variations = [];
      foreach ($product->getVariations() as $variation) {
        $old_variations[$variation->id()] = $variation;
      }

      // Get product data including variants.
      $result = $this->pf->syncProducts('@' . $printful_id);
      $sync_variants = $result['result']['sync_variants'];
    }
    $printful_product_id = $sync_variants[0]['product']['product_id'];
    // Get product variants.
    $variant_data = $this->pf->products($printful_product_id);

    $variation_bundle = $this->entityTypeManager->getStorage('commerce_product_type')->load($product->bundle())->getVariationTypeId();
    foreach ($sync_variants as $printful_variant) {
      // Get required params from $variant_data.
      $variant_params = array_filter($variant_data['result']['variants'], function ($variant) use ($printful_variant) {
        return ($variant['id'] == $printful_variant['product']['variant_id']);
      });
      $variant_params = array_values($variant_params)[0];

      $variation_id = $this->syncProductVariant($printful_variant, $variant_params, $product, $variation_bundle);
      unset($old_variations[$variation_id]);
    }

    // Delete obsolete, orphaned variations, if any.
    foreach (array_keys($old_variations) as $old_variation_id) {
      if (!isset($variations[$old_variation_id])) {
        $old_variations[$old_variation_id]->delete();
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function syncProductVariant(array $printful_variant, array $variant_parameters, ProductInterface $product, $variation_bundle) {
    $variationStorage = $this->entityTypeManager->getStorage('commerce_product_variation');

    $product_variations = $variationStorage->loadByProperties([
      'printful_reference' => $printful_variant['external_id'],
    ]);

    $isNew = FALSE;

    if (empty($product_variations)) {
      $isNew = TRUE;
      $variation = $variationStorage->create([
        'type' => $variation_bundle,
      ]);
      $variation->save();
      $variation->sku->value = $product->id() . '-' . $variation->id();
      $variation->product_id->target_id = $product->id();
      $variation->printful_reference->printful_id = $printful_variant['external_id'];
      if (isset($variation->commerce_stock_always_in_stock)) {
        $variation->commerce_stock_always_in_stock->setValue(TRUE);
      }
    }
    else {
      $variation = reset($product_variations);
      if (!$this->update) {
        return $variation->id();
      }
    }

    $variation->title->value = $printful_variant['name'];
    $variation->price->setValue(new Price($printful_variant['retail_price'], $printful_variant['currency']));

    // Synchronize mapped variation fields.
    foreach ($this->printfulStore->get('attributeMapping') as $attribute => $field_name) {
      // Image type field.
      if ($attribute === 'image') {
        $file_data = [];
        foreach ($printful_variant['files'] as $file_data) {
          if ($file_data['type'] === 'preview') {
            break;
          }
        }
        if (!empty($file_data)) {
          $this->syncImage($variation, $file_data, $field_name);
        }
        else {
          $this->logger->warning($this->t('Image data missing on variant: @variant', [
            '@variant' => json_encode($printful_variant),
          ]));
        }
      }

      // Attribute field.
      else {
        $this->syncAttribute($attribute, $variation, $variant_parameters, $field_name);
      }
    }

    $variation->save();

    if ($isNew) {
      $product->addVariation($variation);
      $product->save();
    }

    return $variation->id();
  }

  /**
   * Helper function to sync an image.
   *
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   Commerce product variation entity.
   * @param array $file_data
   *   Printful file data.
   * @param string $field_name
   *   The name of the image field.
   */
  protected function syncImage(ProductVariationInterface $variation, array $file_data, $field_name) {
    $field_type = $variation->{$field_name}->getFieldDefinition()->getType();
    switch ($field_type) {
      case 'image':
        // Remove existing images if any.
        foreach ($variation->{$field_name} as $item) {
          $item->delete();
        }

        $file_directory = $variation->{$field_name}->getFieldDefinition()->getSetting('file_directory');

        // Replace tokens in file directory string.
        $file_directory = $this->token->replace($file_directory);

        $uri_scheme = $variation->{$field_name}->getFieldDefinition()->getFieldStorageDefinition()->getSetting('uri_scheme');
        $destination_dir = $uri_scheme . '://' . $file_directory;
        if (!$this->fileSystem->prepareDirectory($destination_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          throw new PrintfulException(sprintf('Variant image target directory (%s) problem.', $destination_dir));
        }

        $destination = $destination_dir . '/' . $file_data['filename'];

        $file = $this->fileRepository->writeData(file_get_contents($file_data['preview_url']), $destination, FileSystemInterface::EXISTS_RENAME);
        if (!$file) {
          throw new PrintfulException('Variant image save problem.');
        }

        $file->save();
        $variation->{$field_name}->setValue([['target_id' => $file->id()]]);

        break;

      case 'entity_reference':
        // Upload the file and create the file entity.
        // Get the bundle allowed by the ER field settings.
        $bundle = reset($variation->{$field_name}->getFieldDefinition()->getSetting('handler_settings')['target_bundles']);

        // In order to save the file, we need to get details
        // from the image field attached to the media item.
        $media_image_field = FALSE;

        // Determine the image field name.
        $field_definitions = $this->entityFieldManager->getFieldDefinitions('media', $bundle);
        foreach ($field_definitions as $field_definition) {
          if ($field_definition->getType() === 'image' && $field_definition->getName() !== 'thumbnail') {
            $media_image_field = $field_definition;
            $this->logger->notice('Setting field name to @name', ['@name' => $media_image_field->getName()]);
            break;
          }
        }

        if (!$media_image_field) {
          throw new PrintfulException('Unable to determine destination field for media entity.');
        }

        $media_image_field_name = $media_image_field->getName();

        $existing = $variation->get($field_name)->referencedEntities();
        // Remove existing file entities first so the filename can be
        // maintained.
        foreach ($existing as $e) {
          if ($existing_fid = $e->getSource()->getSourceFieldValue($e)) {
            if ($existing_file = $this->entityTypeManager->getStorage('file')->load($existing_fid)) {
              $this->logger->notice('Deleting existing file (FID: @fid)', ['@fid' => $existing_fid]);
              $existing_file->delete();
            }
          }
        }

        $file_directory = $media_image_field->getSetting('file_directory');

        // Replace tokens in file directory string.
        $file_directory = $this->token->replace($file_directory);

        $uri_scheme = $media_image_field->getSetting('uri_scheme');
        if (!$uri_scheme) {
          $uri_scheme = 'public';
        }

        $destination_dir = $uri_scheme . '://' . $file_directory;

        if (!$this->fileSystem->prepareDirectory($destination_dir, FileSystemInterface::CREATE_DIRECTORY | FileSystemInterface::MODIFY_PERMISSIONS)) {
          throw new PrintfulException(sprintf('Variant image target directory (%s) problem.', $destination_dir));
        }

        $destination = $destination_dir . '/' . $file_data['filename'];

        $file = FALSE;
        if ($file = $this->fileRepository->writeData(file_get_contents($file_data['preview_url']), $destination, FileSystemInterface::EXISTS_RENAME)) {
          $file->save();
          $this->logger->notice('Created new file (FID: @fid)', ['@fid' => $file->id()]);
        }
        else {
          throw new PrintfulException('Variant image save problem.');
        }

        if ($existing) {
          $media = reset($existing);
          $this->logger->notice('Found existing media (MID: @mid)', ['@mid' => $media->id()]);
          $media->set($media_image_field_name, [
            'target_id' => $file->id(),
            'alt' => $variation->label(),
          ]);
          $this->logger->notice('Adding file to existing media (MID: @mid)', ['@mid' => $media->id()]);
          $media->save();
        }
        else {
          // Create the media entity.
          $media = Media::create([
            'bundle' => $bundle,
            'name' => $variation->label(),
            $media_image_field_name => [
              'target_id' => $file->id(),
              'alt' => $variation->label(),
            ],
          ]);
          // Save the media entity.
          $media->save();
          $this->logger->notice('Created new media (MID: @mid)', ['@mid' => $media->id()]);

          // Set the field value.
          $variation->{$field_name}->setValue([['target_id' => $media->id()]]);
        }
        break;

      default:
        throw new PrintfulException(sprintf('Unsupported image type: %s', $field_type));
    }
  }

  /**
   * Helper function to sync a commerce attribute.
   *
   * @param string $attribute
   *   Attribute name.
   * @param \Drupal\commerce_product\Entity\ProductVariationInterface $variation
   *   Commerce product variation entity.
   * @param array $variant_parameters
   *   Printful variant data array.
   * @param string $field_name
   *   The name of the image field.
   */
  protected function syncAttribute($attribute, ProductVariationInterface $variation, array $variant_parameters, $field_name) {
    // Remove existing values.
    foreach ($variation->{$field_name} as $item) {
      $item->delete();
    }

    // Sometimes the attribute may not be available in more general setups.
    if (isset($variant_parameters[$attribute])) {
      // Get attribute bundle from field name (TODO: no better way to do it?
      // Getting it from handler settings doesn't seem like good idea as well).
      $attibute_value_bundle = substr($field_name, strpos($field_name, '_') + 1);

      $properties = [
        'attribute' => $attibute_value_bundle,
        'name' => $variant_parameters[$attribute],
      ];

      $attributeValueStorage = $this->entityTypeManager->getStorage('commerce_product_attribute_value');
      $result = $attributeValueStorage->loadByProperties($properties);
      if (!empty($result)) {
        $attribute = reset($result);
      }
      else {
        $attribute = $attributeValueStorage->create($properties);
        $attribute->save();
      }

      $variation->{$field_name}[0] = [
        'target_id' => $attribute->id(),
      ];
    }
  }

}
